Classification:

> cor(Smarket [,-9]) # removes last column bc it's qualitative

To cut data to use to test on:
> train=(Year<2005) # is a boolean vector
> Smarket.2005= Smarket[!train,] # IF YOUR TRAIN IS A NUMBER, USE - NOT !
> dim(Smarket.2005)
> Direction.2005= Direction [!train]

1. Logistic Regression

> glm.fits=glm(Direction~Lag1+Lag2+Lag3+Lag4+Lag5+Volume, data=Smarket ,family=binomial) # fits generalized glm linear models, a class of models that
# includes logistic regression. (argument family=binomial to tell R to run a logistic regression; if don't put it, glm() works like lm())
> glm.fits=glm(Direction~Lag1+Lag2+Lag3+Lag4+Lag5+Volume , data=Smarket ,family=binomial ,subset=train) # with subset
> coef(glm.fits) # to access just the coefficients for this fitted model
> summary (glm.fits)$coef # returns only table with coefficients and p value
> summary (glm.fits)$coef[,4]

# Next, we are TESTING!!
> glm.probs=predict (glm.fits,type="response") #  predict the probability that the market will go up, given values of the predictors.
# If no data set is supplied to the predict() function, then the probabilities are computed for the training data that was used to fit the logistic regression model
# So if we want to test on ANOTHER dataset than the one we used to train on:
> glm.probs=predict (glm.fits,Smarket.2005, type="response") # Smarket.2005 is the data we're testing on
> glm.probs [1:10]
# We know that these values correspond to the probability of the market going up, rather than down, because the contrasts()
# function indicates that R has created a dummy variable with a 1 for Up
> contrasts (Direction)

# to make a prediction as to whether the market will go up or down on a particular day, we must convert these predicted
# probabilities into class labels, Up or Down.
> glm.pred=rep("Down" ,1250) # or NO or 0 or anything NEGATIVE
> glm.pred[glm.probs >.5]="Up"
# these two commands create a vector of class predictions based on whether the predicted probability of a market increase is greater than or less than 0.5.

> table(glm.pred,Direction) # to produce a confusion matrix in order to determine how many observations were correctly or incorrectly classified
> mean(glm.pred==Direction) # to compute the fraction of days for which the prediction was correct (like accuracy)
> mean(glm.pred!=Direction.2005) # to compute test error rate


> predict (glm.fits,newdata =data.frame(Lag1=c(1.2 ,1.5), Lag2=c(1.1,-0.8) ),type="response") # to predict the returns associated with particular values of the predictors Lag1 and Lag2
# BUT PAY ATTENTION! number of variables used to make glm.fits should be the same as the num of variables in the newdata data frame!

2. Linear Discriminant Analysis (LDA)

> library(MASS)
> lda.fit=lda(Direction~Lag1+Lag2 ,data=Smarket ,subset=train)

> lda.pred=predict (lda.fit , Smarket.2005) # returns a list with three elements. "class" contains LDA's predictions about the movement of the market.
# "posterior" is a matrix whose kth column contains the posterior probability that the corresponding observation belongs to the kth class, (Pk(x)).
# Finally, "x" contains the linear discriminants, described earlier.
> names(lda.pred) # gives u class (Up/Down), posterior, and x.
> lda.class=lda.pred$class
> table(lda.class ,Direction.2005) # realize that LDA and logistic regression predictions are almost identical.
> sum(lda.pred$posterior[,1]>=.5) # Applying a 50 % threshold to the posterior probabilities allows us to recreate the predictions contained in lda.pred$class.
> sum(lda.pred$posterior[,1]<.5)
> mean(lda.class==Direction.2005)

#Notice that the posterior probability output by the model corresponds to the probability that the market will decrease!!
#because the prob < 0.5 are Up in the class, this shows that these prob don't represent the market going Up, but Down.
> sum(lda.pred$posterior[,1]>.9) # want to predict a market decrease only if we are very certain that the market will indeed decrease on that day-say, if the posterior probability is at least 90 %.

3. Quadratic Discriminant Analysis

> qda.fit=qda(Direction~Lag1+Lag2 ,data=Smarket ,subset=train)
> qda.class=predict (qda.fit ,Smarket.2005) $class
> table(qda.class ,Direction.2005)
> mean(qda.class==Direction.2005)

4. K-Nearest Neighbors

Need 4 inputs:
1. A matrix containing the predictors associated with the training data, labeled train.X below.
2. A matrix containing the predictors associated with the data for which we wish to make predictions, labeled test.X below.
3. A vector containing the class labels for the training observations, labeled train.Direction below.
4. A value for K, the number of nearest neighbors to be used by the classifier.

> library(class) # load it to use KNN
> train.X=cbind(Lag1 ,Lag2)[train ,]
> test.X=cbind(Lag1 ,Lag2)[!train ,] # BE CAREFUL: if train is number, - not !
OR # if only 1 predictor (want train.X and test.X to be matrices)
> train.X = as.matrix(Lag2[train])
> test.X = as.matrix(Lag2[!train]) # BE CAREFUL: if train is number, - not !
> train.Direction =Direction [train]

> set.seed(1) # because if several observations are tied as nearest neighbors, then R will randomly break the tie. Therefore, a seed must be set in order to ensure reproducibility of results.
> knn.pred=knn(train.X, test.X, train.Direction, k=1) # k =1 not good... try with k=3,...
> table(knn.pred ,Direction.2009)
> mean(Direction.2009==knn.pred)

> standardized.X= scale(Caravan [,-86]) # because the columns have different units/scales, we standardize them (except the qualitative).

To split the data:
> test=1:1000
> train.X= standardized.X[-test ,]
> test.X= standardized.X[test ,]
> train.Y=Purchase [-test]
> test.Y=Purchase [test]
> set.seed(1)
> knn.pred=knn(train.X,test.X,train.Y,k=1) # size of knn.pred = size of test.X bc knn tests on it; so here length(knn.pred) = 1000
> mean(test.Y!=knn.pred) # KNN error rate
> table(knn.pred ,test.Y) # can look at where Yes-Yes, this is the rate that out of customers who are likely to do x, how much actually do it;
# it's more important than the overall error rate (overall how many customers purchased).