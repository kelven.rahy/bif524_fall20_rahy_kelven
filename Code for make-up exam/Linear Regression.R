Linear Regression:
  
1. Simple Linear Regression

> lm.fit=lm(medv~lstat , data=Boston) # fit a simple linear regression model, with medv as the response and lstat as the predictor (or can use attach() and then without data=...)
> summary (lm.fit) # gives intercept, coeff,...
> names(lm.fit)
> coef(lm.fit)
> confint (lm.fit) # to obtain a confidence interval for the coefficient estimates
> predict (lm.fit, data.frame(lstat=c(5,10 ,15)), interval ="confidence") # produces confidence intervals for the prediction of medv for a given value of lstat
# if asked what is predicted mpg associated with horsepower of 98 and find 95% intervals...
> predict(lm.fit, data.frame(horsepower=c(98)), interval="confidence")
> predict(lm.fit, data.frame(horsepower=c(98))) # just get value of mpg at horsepower=98, without interval
> predict (lm.fit ,data.frame(lstat=c(5,10 ,15)), interval ="prediction") # produces prediction intervals for the prediction of medv for a given value of lstat
> predict (lm.fit ,data.frame(lstat=c(5,10 ,15), meds=c(10,23,45)), interval ="prediction") # if have multiple predictors!

> plot(lstat ,medv) # put response AFTER
> abline(lm.fit,lwd=3,col =2) # draws the least squares regression line
> abline(a=-1, b=0.5, col=3) # a is intercept, b is slope
> plot(lm.fit) # get 4 e diagnostic plots of the least squares regression fit
> plot(predict (lm.fit), residuals (lm.fit)) # compute the residuals from a linear regression fit
> plot(predict (lm.fit), rstudent (lm.fit)) # return the studentized residuals, and we can use this function to plot the residuals against the fitted values.
# for rstudent we expect values between -3 and 3; if outside, outlier!

> plot(hatvalues(lm.fit)) # leverage statistics can be computed for any number of predictors by using hatvalues()
> which.max(hatvalues(lm.fit)) # gives us max leverage

# if asked to return rows in which horsepower = 130:
> x=which(auto[,"horsepower"]==130, arr.ind=TRUE)
> auto[x,]
> legend(-1, legend = c("model fit", "pop. regression"), col=2:3, lwd=3)

2. Multiple Linear Regression

> lm.fit=lm(medv~lstat+age, data=Boston) # fits multiple predictors
> lm.fit=lm(medv~.,data=Boston) # performs regression on all predictors
> summary (lm.fit)
> summary(lm.fit)$r.sq # gives R squared
> summary(lm.fit)$sigma # gives RSE (?summary.lm to see what more is available,...)

> lm.fit1=lm(medv~.-age ,data=Boston ) # does linear regression on all except age
OR
> lm.fit1=update(lm.fit , ~.-age)

3. Interaction Terms

> lm(medv~lstat*age ,data=Boston) # lstat*age simultaneously includes lstat, age, and the interaction term lstat�age as predictors

4. Non-linear Transformations of the Predictors

> lm.fit2=lm(medv~lstat+I(lstat^2)) # non-linear transformations of the predictors (I(X^2) means X^2). this is a regression of medv onto lstat and lstat^2.
> lm.fit=lm(medv~lstat)
> anova(lm.fit ,lm.fit2) # use the anova() function to further quantify the extent to which the quadratic fit is superior to the linear fit.
> lm.fit5=lm(medv~poly(lstat ,5)) # produces a fifth-order polynomial fit; shows from degree 1 till 5!!!! not just 5!!
> summary (lm(medv~log(rm),data=Boston)) # uses log instead of poly

5. Qualitative Predictors

> lm.fit=lm(Sales~.+Income:Advertising +Price:Age ,data=Carseats) # fits LR on data, some of which is qualitative, and added some interaction.
> contrasts (ShelveLoc) # returns the coding that R uses for the dummy variables (attach before doing it)

6. Split data:
> lm.fit = lm(Apps~., data=College.train)
> lm.pred = predict(lm.fit, College.test)
> mean((College.test[, "Apps"] - lm.pred)^2) # gets MSE