Exercises:
  
5. Resampling Methods:
  
--> Split sample, fit LR using training, predict default status in validation set, then compute validation set error:
  
train = sample(dim(Default)[1], dim(Default)[1]/2)
# ii.
glm.fit = glm(default ~ income + balance, data = Default, family = binomial, 
              subset = train)
# iii.
glm.pred = rep("No", dim(Default)[1]/2)
glm.probs = predict(glm.fit, Default[-train, ], type = "response")
glm.pred[glm.probs > 0.5] = "Yes"
# iv.
return(mean(glm.pred != Default[-train, ]$default))

--> Create a plot displaying the univariate regression coefficients from (a) on the x-axis, and the multiple regression coefficients from (b) on the y-axis.
  
x = c(coefficients(lm.zn)[2],
        coefficients(lm.indus)[2],
        coefficients(lm.chas)[2],
        coefficients(lm.nox)[2],
        coefficients(lm.rm)[2],
        coefficients(lm.age)[2],
        coefficients(lm.dis)[2],
        coefficients(lm.rad)[2],
        coefficients(lm.tax)[2],
        coefficients(lm.ptratio)[2],
        coefficients(lm.black)[2],
        coefficients(lm.lstat)[2],
        coefficients(lm.medv)[2])
y = coefficients(lm.all)[2:14]
plot(x, y)

--> fit classification models in order to predict whether a given suburb has a crime rate above or below the median.

attach(Boston)
crime01 = rep(0, length(crim))
crime01[crim > median(crim)] = 1
Boston = data.frame(Boston, crime01)

train = 1:(dim(Boston)[1]/2)
test = (dim(Boston)[1]/2 + 1):dim(Boston)[1]
Boston.train = Boston[train, ]
Boston.test = Boston[test, ]
crime01.test = crime01[test]

# Logistic Regression
glm.fit = glm(crime01 ~ . - crime01 - crim, data = Boston, family = binomial, 
              subset = train)
glm.probs = predict(glm.fit, Boston.test, type = "response")
glm.pred = rep(0, length(glm.probs))
glm.pred[glm.probs > 0.5] = 1
mean(glm.pred != crime01.test) # get test error rate

# LDA
lda.fit = lda(crime01 ~ . - crime01 - crim, data = Boston, subset = train)
lda.pred = predict(lda.fit, Boston.test)
mean(lda.pred$class != crime01.test)

# KNN
library(class)
train.X = cbind(zn, indus, chas, nox, rm, age, dis, rad, tax, ptratio, black, 
                lstat, medv)[train, ]
test.X = cbind(zn, indus, chas, nox, rm, age, dis, rad, tax, ptratio, black, 
               lstat, medv)[test, ]
train.crime01 = crime01[train]
set.seed(1)
# KNN(k=1)
knn.pred = knn(train.X, test.X, train.crime01, k = 1)
mean(knn.pred != crime01.test)

--> Write a for loop from i = 1 to i = n; find logistic model; compute posterior prob; predict if market goes up; determine if error was made.

count = rep(0, dim(Weekly)[1])
for (i in 1:(dim(Weekly)[1])) {
  glm.fit = glm(Direction ~ Lag1 + Lag2, data = Weekly[-i, ], family = binomial)
  is_up = predict.glm(glm.fit, Weekly[i, ], type = "response") > 0.5
  is_true_up = Weekly[i, ]$Direction == "Up"
  if (is_up != is_true_up) 
    count[i] = 1
}
sum(count)
mean(count) # get LOOCV estimate for the test error



#LOOCV & k-fold CV
library(boot)
glm.fit = glm(mpg~horsepower ,data=Auto)
cv.err = cv.glm(Auto,glm.fit)
cv.err$delta
cv.error=rep(0,5)
for (i in 1:5){
  glm.fit=glm(mpg~poly(horsepower ,i),data=Auto)
  cv.error[i]=cv.glm(Auto,glm.fit)$delta[1]
  # cv.error[i]=cv.glm(Auto,glm.fit, K = 10)$delta[1]
  #delta[1] is the CV default estimate, delta[2] is the bias-corrected estimate. They might be the same sometimes.
}
num_errors = 0
for(i in 1:nrow(Weekly)){
  glm.fit = glm.fit = glm(Direction ~ Lag1+Lag2, data=Weekly[-i, ], family=binomial)
  prediction = predict(glm.fit, Weekly[i, ], type="response")
  prediction = ifelse(prediction > 0.5, "Up", "Down")
  if(prediction != Weekly[i,]$Direction){
    num_errors = num_errors + 1
    print("Incorrect")
  }
  else {
    print("Correct")
  }
  test_error_estinate = num_errors / nrow(Weekly)
}


library(ipred) #kfoldcv(k, N) #divides N data points into k roughly equally sized groups
library(permute)
library(insight)

## K-fold CV

```{r}
#set.seed(1)

cross_validation = function(dataset, model, K)
{
  #data(dataset)
  #attach(dataset)
  response=find_response(model)
  actual_response=dataset[,response]
  fm = find_formula(model)$conditional
  N=dim(dataset)[1]
  all=1:N
  all=shuffle(all)
  k=K
  partitions = kfoldcv(k, N)
  index=1
  mse=c()
  
  for (i in 1:K)
  {
    test = all[index:(index+partitions[i]-1)]
    train=all[-test]
    #print(length(test))
    
    
    mod=lm(formula = fm, data = dataset[train,])
    
    pred = predict(mod, dataset[test,])
    
    mse[i]=mean((actual_response[test]-pred)^2)
    #mse[i]=sum((actual_response[test]-pred)^2)/partitions[i]
    
    index=index+partitions[i]
  }
  
  #cv=sum(mse*partitions)/N
  cv=sum(mse)/k
  return(cv)
}
#data(Auto)
#attach(Auto)
model=lm(formula = mpg ~ poly(horsepower,1), data = Auto)
print(cross_validation(Auto, model, 392))


## LOOCV

{r}
set.seed(1)

loocv = function(dataset, mod)
{
  #data(dataset)
  #attach(dataset)
  #View(dataset)
  mod1=mod
  response=find_response(mod1)
  actual_response=dataset[,response]
  
  fm = find_formula(mod1)$conditional
  
  all=1:392
  N=nrow(dataset)
  print(N)
  mse=c()
  
  for (i in 1:N)
  {
    train=all[-i]
    mod1=lm(formula = fm, data = dataset[train,])
    
    pred = predict(mod1, dataset[-train,])
    #print(pred)
    
    #mse=mse+(actual_mpg[i]-pred)^2
    mse[i]=(actual_response[i]-pred)^2
  }
  
  cv=mean(mse, na.rm = T)
  return(cv)
  
}
model=lm(formula = mpg ~ poly(horsepower,1), data = Auto)
print(loocv(Auto, model))




--> Set a random seed, and then compute the LOOCV errors
# here have x and y, so create data frame
library(boot)
Data = data.frame(x, y)
set.seed(1)

glm.fit = glm(y ~ x)
cv.glm(Data, glm.fit)$delta

--> (a) Based on this data set, provide an estimate for the population mean of medv. Call this estimate ^??.
        
        mean(medv)

    (b) Provide an estimate of the standard error of ^??. Interpret this result.

        sd(medv)/sqrt(length(medv))
        
    (c) Now estimate the standard error of ^?? using the bootstrap. How does this compare to your answer from (b)?
        
        boot.fn = function(data, index) return(mean(data[index]))
        bstrap = boot(medv, boot.fn, 1000)
        
    (d) Provide an estimate for the tenth percentile of medv
        
        medv.tenth = quantile(medv, c(0.1))
        
    (e) Use the bootstrap to estimate the standard error of ^??0.1.
        
        boot.fn = function(data, index) return(quantile(data[index], c(0.1)))
        boot(medv, boot.fn, 1000)