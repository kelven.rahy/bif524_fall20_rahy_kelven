Introduction:
  
site: http://faculty.marshall.usc.edu/gareth-james/ISL/
  
Load dataset:
Auto = ISLR::Auto
  
Read data from file:
  
> read.table(file, header = FALSE, sep = "", dec = ".") # Read tabular data into R
> read.csv(file, header = TRUE, sep = ",", dec = ".", ...) # Read "comma separated value" files (".csv")
> read.csv2(file, header = TRUE, sep = ";", dec = ",", ...) # use when comma as decimal point and a semicolon as field separator.
> read.delim(file, header = TRUE, sep = "\t", dec = ".", ...) # Read TAB delimited files
#file: the path to the file containing the data to be imported into R.
#sep: the field separator character. "\t" is used for tab-delimited file.
#header: logical value. If TRUE, read.table() assumes that your file has a header row, so row 1 is the name of each column. If that's not the case, you can add the argument header = FALSE.
#dec: the character used in the file for decimal points.

> library("readxl") # read xls files
> my_data <- read_excel("my_file.xls")
> my_data = read_excel("College.xlsx", col_names=TRUE, na="?")

# to get indices of outliers:
> boxplot(dat)$out
> which(dat %in% boxplot(dat)$out)
  
NA:
> indx = apply(college, 2, function(x) any(is.na(x))) # to find if have NA in every column
> colnames[indx]

# to remove columns using names:
> library(dplyr)
> select(ocd, select=-c("OCI...High.Low"))

# to replace
OCD$OCI = replace_na(OCD$OCI, "Stay!")

# to remove all rows that have NA
Hitters =na.omit(Hitters)

1. Basic Commands
> x = c(1,3,2,5) # creates vector
> length(x)
> x+y # adds 2 vectors
> ls() # loads a list of all of the objects
> rm(x,y) # removes
> rm(list=ls()) # removes all objects

> matrix (c(1,2,3,4) ,2,2,byrow=TRUE) # creates matrix and adds by row
> sqrt(x)
> x^2

> x=rnorm(50) # generates a vector of random normal variables
> y=x+rnorm(50,mean=50,sd=.1)
> cor(x,y) # finds correlation
> cor(subset(Auto, select=-name)) # correlation matrix: finds correlations between all variables except name
> set.seed(1303) # makes code produce same set of random numbers (put it before do random)
> mean(y)
> var(y)
> median(y)
> sqrt(var(y))
> sd(y)
> colMeans(Auto[sapply(Auto, is.numeric)]) # gets mean of every predictor (every column)
OR
> colMeans(Auto[,1:8])
> sapply(Auto[sapply(Auto, is.numeric)], range) # gets range of every column (after checking that it's quantitative)
OR
> sapply(Auto[,1:8], range) 
> sapply(Auto[sapply(Auto, is.numeric)], sd) # gets standard deviation

> Elite = rep("No",nrow(college)) # creates vector with repeated "No", nrow times; called binning
> Elite[college$Top10perc >50]=" Yes" # changes all values in Elite to Yes if the condition is true
> Elite=as.factor(Elite) # if as.factor doesnt work, use factor!!!
> college=data.frame(college , Elite) # to add to column

par(mfrow=c(2,5))
for (i in 1:length(plotdata)) {
  boxplot(plotdata[,i], main=names(plotdata[i]), type="l")
}

> dim(subset(Boston, chas == 1)) # gets dimension of subset of data where the column =1

# if asked to get the values of predictors of suburbs that have minimum medv:
> t(subset(Boston, medv == min(Boston$medv))) # first medv==min... gives boolean array of size nrow, then subset from the whole data, then t to flip
# columns and rows

# if asked how many ... have value >...?
> dim(subset(Boston, rm > 7))
OR
> length(which(rm>7))

# to create 2 samples where sum(X^2)=sum(Y^2):
> set.seed(1)
> x <- rnorm(100)
> y <- -sample(x, 100)

TO CHANGE FROM QUANTITATIVE TO QUALITATIVE:
> Boston$chas <- factor(Boston$chas, labels = c("N","Y"))

> ahestats <- t.test(cps04$ahe, conf.level = 0.95) # t-test
> ahestats$conf.int

# create simulated data set with p =20 and n=1,000
> set.seed(1)
> p = 20
> n = 1000
> x = matrix(rnorm(n * p), n, p)

2. Graphics

> plot(x,y,xlab="this is the x-axis",ylab="this is the y-axis", main="Plot of X vs Y") # plots
> pdf("Figure.pdf") # (jpeg() for jpeg)
> plot(x,y,col="green")
> plot(Private, Outstate) # produces side-by-side boxplots of Outstate versus Private (if doesnt work, switch them)
# if it doesnt work, try doing as.factor for the qualitative; if not:
> boxplot(Outstate[college$Private=="Yes"], Outstate[college$Private=="No"], names=c("Yes", "No"), ylab="Outstate")
> dev.off() # indicates to R that we are done creating the plot
> seq(0,1,length=10) # makes a sequence of 10 numbers that are equally spaced between 0 and 1.(3:11 is the same as seq(3,11) for integers)
> f=outer(x,y,function (x,y)cos(y)/(1+x^2))
> contour(x,y,f) # to represent three-dimensional data
> image(x,y,fa) # works the same way as contour(), except that it produces a color-coded plot whose colors depend on the z value. This is known as a heatmap.
> persp(x,y,fa) # used to produce a three-dimensional plot.
> par(mfrow=c(2,2)) # divide the print window into four regions so that four plots can be made simultaneously (write it before)


3. Indexing Data

> A=matrix (1:16,4,4)
> A[2,3]
> A[c(1,3),c(2,4)]
> A[1:3,2:4]
> A[1:2,]
> A[,1:2]
> A[-c(1,3) ,]
> dim(A)

4. Loading Data

> Auto=read.table("Auto.data") #  loads the Auto.data file into R and store it as an object called Auto, in a format referred to as a data frame
> fix(Auto) # to view it in a spreadsheet like window

# R has assumed that the variable names are part of the data and so has included
#them in the first row. The data set also includes a number of missing
#observations, indicated by a question mark ?.
> Auto=read.table("Auto.data",header=T,na.strings ="?") # header=T n tells R that the first line of the file contains the variable names
# na.strings tells R that any time it sees a particular character or set of characters (such as a question mark), it should be treated as a missing element of the data matrix

> Auto=read.csv("Auto.csv",header=T,na.strings ="?") # Excel is a common-format data storage program. An easy way to load such
# data into R is to save it as a csv (comma separated value) file and then use the read.csv() function to load it in.
> fix(Auto)
> dim(Auto)
> Auto=na.omit(Auto) # removes rows contain missing observations
> dim(Auto) # again to check if it removed
> names(Auto) # gives variable names

# sometimes we don't want to treat the first column as data, so:
> rownames (college )=college [,1] # gives rownames to college, adds a column. now we have to remove the column already there
> college =college [,-1]

5. Additional Graphical and Numerical Summaries

> plot(Auto$cylinders , Auto$mpg ) # can't just write cylinders, mpg bc R won't know they're variable names
> attach(Auto)
> plot(cylinders , mpg) # tells R to make the variables in this data frame available by name.

# The cylinders variable is stored as a numeric vector, so R has treated it
# as quantitative. However, since there are only a small number of possible
# values for cylinders, one may prefer to treat it as a qualitative variable.
# The as.factor() function converts quantitative variables into qualitative variables.
> cylinders =as.factor(cylinders) # if doesn't work, use factor()!!
> cylinders = as.numeric(cylinders) # convert from categorical to continuous.
> plot(cylinders , mpg , col ="red", varwidth =T, xlab="cylinders ", ylab="MPG") # If the variable plotted on the x-axis is categorical,
# then boxplots will automatically be produced by the plot() function.

> hist(mpg ,col=2, breaks =15) #  plots a histogram (col=2 is the same as col="red")

> pairs(Auto) # creates a scatterplot matrix i.e. a scatterplot for every pair of variables for any given data set
> pairs(~ mpg + displacement + horsepower + weight + acceleration , Auto) # to produce scatterplots for just a subset of the variables.
NOTE
cant do pairs on data frame, have to convert to matrix:
> pairs(data.matrix(college))

> plot(horsepower ,mpg)
> identify (horsepower ,mpg ,name) # provides a useful interactive method for identifying the value for a particular variable for points on a plot
# args: the x-axis variable, the y-axis variable, and the variable whose values we would like to see printed for each point.
# Then clicking on a given point in the plot will cause R to print the value of the variable of interest.

> summary(Auto) # produces a numerical summary of each variable in a particular data set.
> summary (mpg) # summary for just 1 variable